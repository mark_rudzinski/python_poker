# Player class defines a hand and controls chips value
import random
class Player:
	def __init__(self, owner, chipsValue):
		self._ownerHand = []
		self._owner = owner
		self._chipsValue = chipsValue
	
	def getOwner(self):
		return self._owner

	def getChipsValue(self):
		return self._chipsValue
		
	def setChipsValue(self, amount):
		if amount != -1:
			self._chipsValue -= amount
	
	def winChipsValue(self, amount):
		self._chipsValue += amount
	
	def getHandInfo(self):
		value = []
		suit = []
		for i in range(5):
			value.append(self._ownerHand[i]._value) 
			suit.append(self._ownerHand[i]._suit)
		return value, suit
		
	def compTrade(self, deck, level):
		if level == 0:
			amount = 0
			x = True
			self._ownerHand.sort(key=lambda value: value._value, reverse = True)
			while len(self._ownerHand) > 0 and x == True:
				if self._ownerHand[len(self._ownerHand) - 1]._value < 9:
					self._ownerHand.pop(len(self._ownerHand) - 1)
					amount += 1
				else:
					x = False
			self.generateHand(deck, amount)
					
	def generateHand(self, deck, amount):			#generates cards from deck to hands
		for i in range(amount):
			index = random.randrange (len(deck))
			newCard = deck.pop(index)
			self._ownerHand.append(newCard)
		self._ownerHand.sort(key=lambda value: value._value)
		
	def discardHand(self):				#resets hand
		self._ownerHand[:] = []
			
	def displayHand(self, amount):					#displays current hand 
		if len(self._ownerHand) == 0:
				return
		print("\n" + self._owner + "'s hand:")
		for i in range(amount):
				print("  " + str(i+1) + ". " + self._ownerHand[i].getCardInfo())
				
	def trade(self, deck):					#trade cards in hand
		finish = False
		amount = 0
		while finish == False:
			if len(self._ownerHand) == 0:
				break
			try:
				disCard = eval(input("\nDiscard which cards(type 0 when finished)?: "))
				if disCard >= 1 and disCard <= len(self._ownerHand):
					self._ownerHand.pop(disCard - 1)
					amount += 1
					self.displayHand(len(self._ownerHand))	
				elif disCard == 0: 
					finish = True
			except:
				print("Must be a numeric from 0 to 5")
		self.generateHand(deck, amount)
		self.displayHand(5)
		
'''
Poker Project
CSIT 216

Mark Rudzinski		597 lines
Dorothy Carter		9 lines
Gordon Healey		0 lines

'''
def runRound(pot, player1, player2, counter, min):
	if player1.getChipsValue() < min:
		input("You Lost the Game(Hit Enter to Exit).")
		finish = True
		return finish, pot, player1, player2, counter
	elif player2.getChipsValue() < min:
		input("You Won the Game(Hit Enter to Exit).")
		finish = True
		return finish, pot, player1, player2, counter
	counter += 1
	finish = False
	input("(Press Enter for the next round)")
	print("*******************")
	print("Round ", counter)
	print("*******************")
	player1.discardHand()
	player2.discardHand()
	deck = NewDeck()
	deck = deck.getDeck()
	player1.generateHand(deck, 5)
	player1.displayHand(5)
	player2.generateHand(deck, 5)
	compTurn = AI(player2)
	playerChecked = False
	
	#Player is big blind (even rounds)
	if counter % 2 == 0:
		player1.setChipsValue(min)
		print("You are big blind.")
		pot.addToPot(min)
		#turn 1 of betting
		chipsAI = compTurn.betting(pot.getPot(), min, player2.getChipsValue(), playerChecked)
		if chipsAI == -1:
			player1.winChipsValue(pot.resetPot())
			return finish, pot, player1, player2, counter
		elif chipsAI >= min:
			player2.setChipsValue(chipsAI)
			pot.addToPot(chipsAI)
			print ("\nYour chips value: " + str(player1.getChipsValue()))
			chips = pot.makeBet(player1._chipsValue, 1)
			player1.setChipsValue(chips)
			if chips == -1:
				player2.winChipsValue(pot.resetPot())
				return finish, pot, player1, player2, counter
			else:
				action = compTurn.callFold(pot.getPot(), min, player2.getChipsValue())
				if action == 0:
					player2.setChipsValue(chips)
					pot.addToPot(chips)
				else:
					player1.winChipsValue(pot.resetPot())
					return finish, pot, player1, player2, counter
		#turn 2 of betting
		player1.displayHand(5)
		player1.trade(deck)
		player2.compTrade(deck, compTurn.getLevel())
		chipsAI = compTurn.betting(pot.getPot(), min, player2.getChipsValue(), playerChecked)
		if chipsAI == -1:
			player1.winChipsValue(pot.resetPot())
			return finish, pot, player1, player2, counter
		else:
			player2.setChipsValue(chipsAI)
			pot.addToPot(chipsAI)
			print ("\nYour chips value: " + str(player1.getChipsValue()))
			if chipsAI > 0:
				x = 1
			else:
				x = counter
			chips = pot.makeBet(player1._chipsValue, x)
			if chips == -1:
				player2.winChipsValue(pot.resetPot())
				return finish, pot, player1, player2, counter
			else:
				action = compTurn.callFold(pot.getPot(), min, player2.getChipsValue())
				if action == 0:
					player2.setChipsValue(chips)
					pot.addToPot(chips)
				else:
					player1.winChipsValue(pot.resetPot())
					return finish, pot, player1, player2, counter		
	#Computer is big blind (odd rounds)
	else:
		player2.setChipsValue(min)
		print("Computer is big blind.")
		pot.addToPot(min)
		#turn 1 of betting
		print ("\nYour chips value: " + str(player1.getChipsValue()))
		chips = pot.makeBet(player1._chipsValue, counter)
		player1.setChipsValue(chips)
		if chips == -1:
			player2.winChipsValue(pot.resetPot())
			return finish, pot, player1, player2, counter
		else:
			chipsAI = compTurn.betting(pot.getPot(), min, player2.getChipsValue(), playerChecked)
			if chipsAI == -1:
				player1.winChipsValue(pot.resetPot())
				return finish, pot, player1, player2, counter
			elif chipsAI > 0:
				player2.setChipsValue(chipsAI + chips - min)
				pot.addToPot(chipsAI + chips - min)
				pot.displayPot()
				print ("\nYour chips value: " + str(player1.getChipsValue()))
				print("Your options are: ")
				print("\t1.Call")
				print("\t2.Fold")
				finished2 = False
				while finished2 == False:
					choice = input("Your Choice: ") 
					if choice == "1":
						player1.setChipsValue(chipsAI)
						pot.addToPot(chipsAI)
						finished2 = True
					elif choice == "2":
						player2.winChipsValue(pot.resetPot())
						return finish, pot, player1, player2, counter
					else:
						input("Invalid Choice(Enter to continue).")
		#turn 2 of betting		
		player1.displayHand(5)
		player1.trade(deck)
		player2.compTrade(deck, compTurn.getLevel())
		print ("\nYour chips value: " + str(player1.getChipsValue()))
		chips = pot.makeBet(player1._chipsValue, 0)
		player1.setChipsValue(chips)
		if chips == -1:
			player2.winChipsValue(pot.resetPot())
			return finish, pot, player1, player2, counter
		else:
			if chips == 0:
				playerChecked = True
			chipsAI = compTurn.betting(pot.getPot(), min, player2.getChipsValue(), playerChecked)
			if chipsAI == -1:
				player1.winChipsValue(pot.resetPot())
				return finish, pot, player1, player2, counter
			elif chipsAI > 0:
				player2.setChipsValue(chipsAI + chips)
				pot.addToPot(chipsAI + chips)
				pot.displayPot()
				print ("\nYour chips value: " + str(player1.getChipsValue()))
				print("Your options are: ")
				print("\t1.Call")
				print("\t2.Fold")
				finished2 = False
				while finished2 == False:
					choice = input("Your Choice: ") 
					if choice == "1":
						player1.setChipsValue(chipsAI)
						pot.addToPot(chipsAI)
						finished2 = True
					elif choice == "2":
						player2.winChipsValue(pot.resetPot())
						return finish, pot, player1, player2, counter
					else:
						input("Invalid Choice(Enter to continue).")
	cardValues1, suit1 = player1.getHandInfo()
	cardValues2, suit2 = player2.getHandInfo()
	player2.displayHand(5)
	compare1 = Compare(cardValues1, suit1, cardValues2, suit2)
	winner = compare1.runCompare()
	potAmount = pot.resetPot()
	if winner == 0:
		print("You win! Pot was: ", potAmount, "chips")
		player1.winChipsValue(potAmount)
	elif winner == 1:
		print("They win! Pot was: ", potAmount, "chips")
		player2.winChipsValue(potAmount)
	else:
		print("It's a draw. Pot was: ", potAmount, "chips")
		player1.winChipsValue(potAmount/2)
		player2.winChipsValue(potAmount/2)
	return finish, pot, player1, player2, counter
	
def main():
	pot = Pot()
	PName = input("What is your name: ")
	StartChips = 2000
	MinBet = 50
	player1 = Player(PName, StartChips)
	player2 = Player("Computer", StartChips)
	finish = False
	counter = 0				#big blind decider
	while finish == False:
		finish, pot, player1, player2, counter = runRound(pot, player1, player2, counter, MinBet)
	
	again = input("\nPlay again(Y/N)?: ")
	if again =="N" or again =="n":
		return False
	else:
		return True

from Deck import NewDeck
from Pot import Pot
from Player import Player
from Compare import Compare
from AI import AI
runGame = True
while runGame == True:
	runGame = main()
class AI:
	def __init__(self, comp):
		self._comp = comp
		self._level = self.level()
	
	def getLevel(self):
		return self._level
	
	def level(self):
		from Compare import Compare
		cardValues1 = []
		suit1 = []
		cardValues2, suit2 = self._comp.getHandInfo()
		compare = Compare(cardValues1, suit1, cardValues2, suit2)
		return compare.getLevel(cardValues2, suit2)

	def betting(self, pot, minimum, money, playerChecked):
		if self._level == 0 and pot == minimum: #comp call(you are big blind)
			print("Computer Calls.")
			return minimum
		elif self._level == 0 and pot == (minimum * 2): #comp checks(comp is big blind)
			print("Computer Checks.")
			return 0
		elif (1 + (self._level * self._level)) * (minimum * 10) < pot and playerChecked == False:	#comp folds
			print("Computer Folds.")
			return -1
		import random
		correspondences = {0:1,1:1,2:2,3:2,4:2,5:3,6:3,7:3,8:3} 
		amountBet = correspondences[self._level] * (random.randint(1,5) * minimum)
		if amountBet < minimum:
			if minimum > money:
				amountBet = 0
			else:
				amountBet = minimum
		print("The Computer raises ", amountBet, " Chips")
		return amountBet
		
	def callFold(self, pot, minimum, money):
		if (1 + (self._level * self._level)) * (minimum * 5) < pot:	#comp folds
			print("Computer Folds.")
			return -1
		else:
			print("Computer Calls.")
			return 0
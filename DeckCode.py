# Deck class which creates card objects
class NewDeck:
	def __init__(self):
		self._newDeck = []		#local list
		self._suit = ("♥ Hearts","♦ Diamonds","♣ Spades","♠ Clubs")
		self._name = ("Jack","Queen","King","Ace")
	def getDeck(self):

		for i in range(4):
			suit = self._suit[i]	#assign suit
			for j in range(13):
				# assign face 
				if j >= 9:
					name = self._name[j - 9]
				else:
					name = str(j + 2)
				card = Card(name, suit, j + 2)		#makes a card object
				self._newDeck.append(card)		#adds to deck list
		return self._newDeck
		
# Card class defines a card Object
class Card:
	def __init__(self, name, suit, value):
		self._name = name
		self._suit = suit
		self._value = value
	
	def __repr__(self):
		return repr((self._name, self._suit, self._value))
	
	def getCardValue(self):
		return self._value
	
	def getCardInfo(self):				#simple display format
		cardInfo = "The " + self._name + " of " + self._suit
		return cardInfo
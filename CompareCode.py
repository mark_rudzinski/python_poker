class Compare:		# Compares the hands to calculate a winner
	def __init__(self, cardValues1, suit1, cardValues2, suit2):
		self._cardValues1 = cardValues1
		self._cardValues2 = cardValues2
		self._suit1 = suit1
		self._suit2 = suit2
		self._handLevel = ("a high card", "one pair", "two pair", "three of a kind", "a straight", 
		"a flush", "a full house", "four of a kind", "a straight flush")
		
	def isStraight(self, cardValues):
		count = 0
		for i in range(4):
			if (cardValues[i] + 1) == (cardValues[i + 1]):
				count += 1
		if count == 4:
			return True
		else:
			return False
		
	def isFlush(self, suit):
		if len(set(suit)) == 1:
			return True
		else: 
			return False

	def hasAce(self, cardValues):		#makes the Ace work in a low straight
		if cardValues[4] == 14 and cardValues[3] == 5:
			count = 0
			for i in range(3):
				if (cardValues[i] + 1) == (cardValues[i + 1]):
					count += 1
			if count == 3:
				temp = cardValues.pop()
				cardValues.insert(0,1)

	def threeCards(self, cardValues):	#returns 1 if at least 3 of a kind, 2 if 4 of a kind
		count = 0
		for i in range(3):
			if (cardValues[i]) == (cardValues[i + 1]) and (cardValues[i]) == (cardValues[i + 2]):
				count += 1
		return count
		
	def getLevel(self, cardValues, suit):	#returns handLevel index
		level = 0	
		self.hasAce(cardValues)
		#print (cardValues)		#testing purposes
		#print (suit)
		flush = self.isFlush(suit)
		straight = self.isStraight(cardValues)
		
		if len(set(cardValues)) == 5: 
			if straight == True and flush == True:		#straight flush
				level = 8
			elif straight == False and flush == True:		#flush
				level = 5
			elif straight == True and flush == False:		#straight
				level = 4
			else:				#high card
				level = 0
		elif len(set(cardValues)) == 4:		#1 pair
			level = 1
		elif len(set(cardValues)) == 3:		#3 of a kind or 2 pair
			level = 2 + self.threeCards(cardValues)
		elif len(set(cardValues)) == 2:
			level = 5 + self.threeCards(cardValues)		#full house or 4 of a kind

		return level 		

	def getHighCard(self, cardValues1, cardValues2):
		for i in range(4,0,-1):
			if cardValues1[i] > cardValues2[i]:
				return 0
			elif cardValues1[i] < cardValues2[i]:
				return 1
		return 2
		
	def get1Pair(self, cardValues1, cardValues2):
		for i in range(4):
			if (cardValues1[i]) == (cardValues1[i + 1]):
				temp1 = cardValues1[i]
			if (cardValues2[i]) == (cardValues2[i + 1]):
				temp2 = cardValues2[i]
		if temp1 > temp2:
			return 0
		elif temp1 < temp2:
			return 1
		else:
			return self.getHighCard(cardValues1, cardValues2)
			
	def get2Pair(self, cardValues1, cardValues2):
		for i in range(4,1,-1):
			if (cardValues1[i]) == (cardValues1[i - 1]):
				temp1 = cardValues1[i]
			if (cardValues2[i]) == (cardValues2[i - 1]):
				temp2 = cardValues2[i]
		if temp1 > temp2:
			return 0
		elif temp1 < temp2:
			return 1
		else:
			return self.get1Pair(cardValues1, cardValues2)

	def get3Kind(self, cardValues1, cardValues2):
		for i in range(3):
			if (cardValues1[i]) == (cardValues1[i + 1]) and (cardValues1[i]) == (cardValues1[i + 2]):
				temp1 = cardValues1[i]
			if (cardValues2[i]) == (cardValues2[i + 1]) and (cardValues2[i]) == (cardValues2[i + 2]):
				temp2 = cardValues2[i]	
		if temp1 > temp2:
			return 0
		elif temp1 < temp2:
			return 1
		else:
			return 0
				
	def compare(self, cardValues1, cardValues2, level):
		runLevel = {
			0 : self.getHighCard,
			1 : self.get1Pair,
			2 : self.get2Pair,
			3 : self.get3Kind,
			4 : self.getHighCard,
			5 : self.getHighCard,
			6 : self.get3Kind,
			7 : self.get3Kind,
			8 : self.getHighCard
			}
		return runLevel[level](cardValues1, cardValues2)
	'''		dictionary			level
			----------			-----
			0 : getHighCard, 	highcard
			1 : get1Pair,		1 pair
			2 : get2Pair,		2 pair
			3 : get3Kind,		3 of a kind
			4 : getHighCard,	straight
			5 : getHighCard,	flush
			6 : get3Kind,		fullhouse
			7 : get3Kind,		4 of a kind
			8 : getHighCard		straight flush
'''

	def runCompare(self):	
		level1 = self.getLevel(self._cardValues1, self._suit1)
		level2 = self.getLevel(self._cardValues2, self._suit2)	
		print("You have " + self._handLevel[level1])
		print("They have " + self._handLevel[level2])

		if level1 > level2:			#if your hand level is higher
			return 0
		elif level1 < level2:		#if their hand level is higher
			return 1
		elif level1 == level2:		#if both levels are equal
			winner = self.compare(self._cardValues1, self._cardValues2, level1)  #runs compare; see which card value(s) are greater
			return winner

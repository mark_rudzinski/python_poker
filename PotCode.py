# Pot class defines a pot and controls betting
class Pot:
	def __init__(self):
		self._pot = 0
		self._betActions = ("1. Raise", "2. Fold", "3. Check")
		self._betOptions = ("1. Minimum $50", "2. Double $100", "3. Triple $150",
			"4. All In", "5. Custom Bet", "6. Fold")
		
	def getPot(self):
		return self._pot
	
	def addToPot(self, amount):
		self._pot += amount
		
	def displayPot(self):		
		print("\nThe pot value is: " + str(self._pot))
	
	def resetPot(self):
		win = self._pot
		self._pot = 0
		return win
		
	def displayBetAct(self, actions):		#displays the bettting actions(self._betActions)
		print("Betting actions are: ")
		for i in range (actions):
			print("  " + self._betActions[i])
	
	def displayBetOpt(self, chipsValue):	#displays the betting options(self._betOptions)
		finish = False
		
		while finish == False:
			print ("\nYour chips value: " + str(chipsValue))
			print("Betting options are: ")
			for i in range (6):
				print("  " + self._betOptions[i])
		 
			choice = input("Your choice: ")
			if choice == "1" or choice == "2" or choice == "3":
				splash = eval(choice) * 50
				if splash > chipsValue: 
					input("\nYou can not afford that bet(Enter to continue).")
				else:
					self._pot += splash
					return splash
			elif choice == "4":
				self._pot += chipsValue
				return chipsValue
			elif choice == "5":	
				try:
					splash = eval(input("Custom bet amount: "))
					if splash < 50:
						input("\nMinimum bet is 50(Enter to continue).")
					elif splash > chipsValue: 
						input("\nYou can not afford that bet(Enter to continue).")
					else:
						self._pot += splash
						return splash
				except: 
					input("Invalid Bet(Enter to continue).")
			elif choice == "6":
				print("You fold.")
				return -1
			else:
				input("Invalid choice(Enter to continue).")
			
	def makeBet(self, chipsValue, counter):		#goes through the betting phase
		if chipsValue < 50:
			print("You can not bet.")
			return 0
		finish = False
		self.displayPot()
		actions = 3
		if counter % 2 != 0:
			actions = 2
		while finish == False:			
			self.displayBetAct(actions)
			choice = input("Your choice: ")
			if choice == "1":
				splash = self.displayBetOpt(chipsValue)
				self.displayPot()
				return splash
			elif choice == "2":
				print("You fold.")
				return -1
			elif choice == "3" and counter % 2 == 0:
				print("You check.")	
				return 0
			else:
				input("Invalid choice(Enter to continue).")
		